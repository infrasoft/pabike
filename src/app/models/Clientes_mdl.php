<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 *  Modelo para el manejo de clientes
 */
class Clientes_mdl extends CI_Model 
{
	private $table = "";
	function __construct()
	{
		parent::__construct();	
	}
	
	public function consulta($data='')
	{
		
	}
	
	public function alta($data=array())
	{
		$this->db->insert($this->table, $data);
      	return $this->db->insert_id();
	}
	
	public function modifica($data=array())
	{
		return	$this->db->update($this->table, $data);
	}
}
