<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');
/**
 *  Clase para el registro de productos
 */
class Productos_mdl extends CI_Model
{
	private $table = "productos";	
	function __construct()
	{
		parent::__construct();		
	}
	
	public function nuevo($data = array())
	{
	  $this->db->insert($this->table, $data);
      return $this->db->insert_id();		 
	}
	
	public function consulta($data=null)
	{
		$this->db->select('*');
		$this->table = "view_productos";
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}
		return $consulta = $this->db->get();// $consulta->result(); 
	}
	
	public function modificar($data,$id)
	{
		$this->db->where("id",$id);
		return	$this->db->update($this->table, $data);
	}
}

?>
