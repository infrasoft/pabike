<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Modelo para ventas
 */
class Ventas_mdl extends CI_Model 
{
	private $table = "venta";
	function __construct() 
	{
		parent::__construct();
	}
	
	public function nuevo($data = array())
	{
	  $this->db->insert($this->table, $datos);
      return $this->db->insert_id();		 
	}
	
	public function consulta($data=null)
	{
		$this->db->select('*');
		$this->table = "view_ventas";
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}
		$consulta = $this->db->get();
		return $consulta->result(); 
	}
	
	public function modificar($data=array())
	{
		return	$this->db->update($this->table, $data);
	}
}


?>