<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 *  Clase para el manejo de ventas
 */
class Servicios extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();	
		
		$this->load->helper('directory');	
	}
	
	//pagina de inicio de servicios
	public function index($sitio="servicios")
	{
		$data = array();
		$this->load->view("header",$data);
		$this->load->view("header-main");
		$this->load->view("/frontend/".$sitio);
		$this->load->view("footer");
	}
	
	//se muestran el detalles de los servicios brindados
	public function detalles($sitio='servicios')
	{
		$data = array("sitio"=>$sitio);
		$this->load->view("header",$data);
		$this->load->view("header-main");
		$this->load->view("/frontend/servicios/".$sitio,$data);
		$this->load->view("footer");
	}
}

?>