<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 *  Clase para el manejo de ventas
 */
class Sitios extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();	
		
	}
	
	public function index($sitio="home")
	{
		$data = array();
		$this->load->view("header",$data);
		$this->load->view("header-main",$data);
		$this->load->view("/frontend/".$sitio,$data);
		$this->load->view("footer",$data);
	}
}

?>