<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 *  Clase para el manejo de ventas
 */
class Galeria extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();	
		
		$this->load->helper('directory');		
	}
	
	public function index($sitio="galeria")
	{
		$data = array();
		$this->load->view("header",$data);
		$this->load->view("header-main",$data);
		$this->load->view("/frontend/".$sitio,$data);
		$this->load->view("footer");
	}
	
	//detalle de cada una de las galerias
	public function detalle($sitio="galeria")
	{
		$data = array();
		$this->load->view("header",$data);
		$this->load->view("header-main",$data);
		$this->load->view("/frontend/galerias/".$sitio,$data);
		$this->load->view("footer");
	}
}

?>