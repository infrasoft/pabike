 <?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Page Content -->
    <div class="container text-justify">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Institucional
        <small>.</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Institucional</li>
      </ol>

      <!-- Intro Content -->
      <div class="row">
        <div class="col-lg-6">
          <img class="img-fluid rounded mb-4" src="<?=base_url();?>media/img/institucional/logoverde.jpg" alt="">
        </div>
        <div class="col-lg-6">
          <h2>Sobre Nosotros</h2>

          <h3>Visión:</h3>
          <p>Generar felicidad y momentos memorables de Aventura en las montañas.</p>
          
          <h3>Misión:</h3> 
          <p>Somos una empresa de turismo alternativo destinada a brindar excelencia en
           ciclo-turismo de montaña a través de profesionalismo y atención personalizada
            al público local y nacional.</p>
            
          <h3>Valores:</h3> 
          <ul>
					<li><p>Satisfacción de nuestros clientes.</p></li> 
					<li><p>Responsabilidad Social: Con nuestros colaboradores, la comunidad
						 y el medio ambiente.</p></li>
					<li><p>Ambiente laboral respetuoso y grato</p></li>
					<li><p>Profesionalismo</p></li>         
          </ul>          
 


        </div>
      </div>
      <!-- /.row -->

      <!-- Team Members -->
      <h2>Historia</h2>
		<p>Pa Bike Salta “CicloTurismo de Montaña & Rent a Bike”, nace como iniciativa de su líder Julio Nazareno Ruiz.</p>
		<p> Cursando su segundo año de estudio de la tecnicatura en turismo, decide presentar un proyecto al gobierno 
			(Fondo Ciudadano de Desarrollo Cultural). Resultando uno de los ganadores pudo comenzar  su sueño de realizar
			 CicloTurismo, haciendo conocer  la provincia de Salta arriba de una bicicleta y en lugares hermosos.</p>
		<h3>Equipo de trabajo</h3>
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100 text-center">
            <img class="card-img-top" src="<?=base_url();?>media/img/institucional/julio.png" alt="">
            <div class="card-body text-justify">
              <h4 class="card-title">Julio Nazareno Ruiz</h4>
              <h6 class="card-subtitle mb-2 text-muted">Administrador</h6>
              <p class="card-text">Julio (Lorito), es el líder de Pa Bike Salta. Técnico en Turismo, Guía de Turismo, amante de la
              	 naturaleza y del Downhill en Mountain Bike.
              	  Ganando proyectos presentados en el Ministerio de Turismo de Salta pudo comenzar su sueño de realizar circuitos de 
              	  baja/media dificultad para que los turistas puedan conocer los paisajes salteños de una forma diferente y vivan 
              	  una experiencia en contacto directo con la naturaleza.
              	 </p>
            </div>
            <div class="card-footer">
              <a href="#"></a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100 text-center">
            <img class="card-img-top" src="<?=base_url();?>media/img/institucional/gaby.png" alt="">
            <div class="card-body text-justify">
              <h4 class="card-title">Gabriela Velozo</h4>
              <h6 class="card-subtitle mb-2 text-muted">Fotógrafa</h6>
              <p class="card-text">Técnica en Turismo, Guía de Turismo. Conoce la mayor parte de los paisajes de Salta, lo que le da
              	 la ventaja de saber donde y cuando disparar su cámara de fotos.
              	 Divertida y muy sociable. Tiene grandes habilidades con el diseño grafico y la cámara de fotos, la encargada de 
              	 captar las imágenes que quedaran para el recuerdo en las excursiones en Mountain Bike.
              	 </p>
            </div>
            <div class="card-footer">
              <a href="#"></a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100 text-center">
            <img class="card-img-top" src="<?=base_url();?>media/img/institucional/abel.png" alt="">
            <div class="card-body text-justify">
              <h4 class="card-title">Julio Abel Ruiz</h4>
              <h6 class="card-subtitle mb-2 text-muted">Conductor</h6>
              <p class="card-text">Abel es el conductor del vehículo de apoyo, dándole así una mano a su hijo. Es especialista en 
              	tabaco (principal producción en la provincia), es el indicado para explicarles a los turistas este importante proceso
				productivo, conjugando su conocimiento, experiencia y anécdotas es un excelente acompañante para las excursiones.
				</p>
            </div>
            <div class="card-footer">
              <a href="#"></a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

      <!-- Our Customers -->
      <h2>Nuestros Clientes</h2>
      <div class="row">
      	<?php
      		$map = directory_map('./media/img/servicios/clientes/', 1);
			foreach ($map as $value) 
			{
				echo "<div class=\"col-lg-2 col-sm-4 mb-4\">
        		  <img class=\"img-fluid img-thumbnail\" src=\"".base_url()."media/img/servicios/clientes/".$value."\" >
        		</div>";	
			}
      	?>
        
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->