<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Contact Form -->
      
      <div class="row">
        <div class="col-lg-8 mb-4">
          <h3>Envie un mensaje</h3>
          <form name="sentMessage" id="contactForm" >
            <div class="control-group form-group">
              <div class="controls">
                <label class="sr-only">Apellido y Nombre:</label>
                <input type="text" class="form-control input-sm" id="name" 
                required data-validation-required-message="Por favor ingrese su nombre"
                placeholder="Apellido y nombre">
                <p class="help-block"></p>
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label class="sr-only">Telefono:</label>
                <input type="tel" class="form-control input-sm" id="phone" 
                required data-validation-required-message="Por favor complete su telefono"
                placeholder="Telefono:">
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label class="sr-only">Email:</label>
                <input type="email" class="form-control input-sm" id="email" 
                required data-validation-required-message="Por favor complete su email"
                placeholder="Email">
                
              </div>
            </div>
            
            <div class="control-group form-group">
            	<div class="controls">
            	<select class="form-control">
                	<option>Consultas</option>
                	<option>Solicitud de presupuestos</option>
                	<option>Eventos</option>
                </select>
            	</div>
            </div>
            
            <div class="control-group form-group">
              <div class="controls">
                <label>Mensaje:</label>
                <textarea rows="10" cols="100" class="form-control input-sm" id="message" 
                required data-validation-required-message="Por favor ingrese su mensaje" 
                
                maxlength="999" style="resize:none"></textarea>
              </div>
            </div>
            <div id="success"></div>
            <!-- For success/fail messages -->
            <button type="submit" class="btn btn-primary" id="sendMessageButton">Enviar</button>
          </form>
        </div>

      </div>
      <!-- /.row -->