<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
  <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Servicios
        <small>.</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Services</li>
      </ol>

      <!-- Image Header -->
      <img class="img-fluid rounded mb-4" src="<?=base_url();?>media/img/servicios/presentacion.jpg" alt="">

      <!-- Marketing Icons Section -->
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header text-justify">
            	<a href="<?=base_url();?>index.php/servicios/detalles/cuesta-del-obispo/">Descenso de la Cuesta del Obispo en MTB</a> </h4>
            <div class="card-body">
              <p class="card-text">10 horas • $1500 x pasajero.</p> 
              <p class="card-text">	 Incluye: Traslado, Bici, Casco, Guantes, Guía, Almuerzo, Hidratacion, Fotos y Seguros Correspondientes. <br/>
              			  Aceptamos tarjetas de Crédito y Débito. </p> 
              <p class="card-text">	  El descenso de la Cuesta del Obispo es una experiencia única, deslumbrante, llena de adrenalina y diversión.
              			   Combinando ruta y sendero recorremos 28 km entre Cuesta y Quebrada de Escoipe.</p>
            </div>
            <div class="card-footer">
              <a href="<?=base_url();?>index.php/servicios/detalles/cuesta-del-obispo/" class="btn btn-primary">Ver mas</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header text-justify">
            	<a href="<?=base_url();?>index.php/servicios/detalles/selva-de-yungas/">Mountain Bike por la Selva de Yungas.</a> </h4>
            <div class="card-body">
              <p class="card-text">6 horas • $900 x persona. </p>
              <p class="card-text">Incluye: Traslado, Bici, Casco, Guantes, Guía, Almuerzo, Hidratacion, Fotos y Seguros 
              			Correspondientes. Aceptamos tarjetas de Crédito y Débito.</p>
			  <p class="card-text">El Camino de Cornisa por la Selva de Yungas es un lugar magnifico para recorrerlo en Mountain Bike.
			  			 Arboles de gran porte, llanas, enredaderas, epifitas y un pintoresco dique nos acompañan 22 km.</p>
            </div>
            <div class="card-footer">
              <a href="<?=base_url();?>index.php/servicios/detalles/selva-de-yungas/" class="btn btn-primary">Ver mas</a>
            </div>
          </div>
        </div>
        
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header text-justify">
            	<a href="<?=base_url();?>index.php/servicios/detalles/quebrada-del-toro/">Quebrada del Toro</a> </h4>
            <div class="card-body">
              <p class="card-text">6 horas • $900 x persona. </p>
              <p class="card-text">Incluye: Traslado, Bici, Casco, Guantes, Guía, Almuerzo, Hidratacion, Fotos y Seguros Correspondiente. Aceptamos tarjetas
              			 de Crédito y Débito.</p>
			  <p class="card-text">Desde Estación Chorrillos sobre RN 51 comenzaremos el recorrido hasta Campo Quijano, en unos 25 km, recorreremos la Puna 
			  			 Salteña y el Valle de Lerma, Acompañando las míticas vías del Tren a las Nubes.</p>
            </div>
            <div class="card-footer">
              <a href="<?=base_url();?>index.php/servicios/detalles/quebrada-del-toro/" class="btn btn-primary">Ver mas</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header text-justify">
            	<a href="<?=base_url();?>index.php/servicios/detalles/logistica/">Logistica</a> </h4>
            <div class="card-body">
              <p class="card-text">Aceptamos tarjetas de Crédito y Débito. </p>
              <p class="card-text">¿Queres recorrer Salta con tu bici o con tu grupo de amigos por tu cuenta?</p>
			  <p class="card-text">Nosotros te recomendamos recorridos, te brindamos apoyo vehicular y todo lo que tenga que ver con logistica. 
			  						Consultanos!!.</p>
            </div>
            <div class="card-footer">
              <a href="<?=base_url();?>index.php/servicios/detalles/logistica/" class="btn btn-primary">Ver mas</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header text-justify">
            	<a href="<?=base_url();?>index.php/servicios/detalles/rent-a-bike/">Rent a Bike</a> </h4>
            <div class="card-body">
              <p class="card-text">1 hora • $60 x hora $450 full day.  </p>
              <p class="card-text">Consultar precios de Mountain Bike y Equipos para viajes en bicicleta. Aceptamos tarjetas de Crédito
              						 y Débito.</p>
			  <p class="card-text">Destinado a turistas responsables en el uso y cuidado de la bicicleta. Que quieran recorrer atractivos
			  							 por su cuenta y realizar viajes por la provincia de Salta y otros puntos turísticos del NOA.</p>
            </div>
            <div class="card-footer">
              <a href="<?=base_url();?>index.php/servicios/detalles/rent-a-bike/" class="btn btn-primary">Ver mas</a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->