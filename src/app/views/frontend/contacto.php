<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Contacto
        <small>Contact</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Contacto</li>
      </ol>

      <!-- Content Row --> 
      <div class="row">
        <!-- Map Column -->
        <div class="col-lg-8 mb-4">
          <!-- Embedded Google Map -->
          <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
          	src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d4976.7263243314255!2d-65.43376451200346!3d-24.78881887960094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1528823708955">
          </iframe>
        </div>
        <!-- Contact Details Column -->
        <div class="col-lg-4 mb-4 card h-100">
          <h3 class="card-title text-justify">Detalles de contacto</h3>
          <p class="card-text">
             Marcelino Cornejo 174
            <br>Salta capital, CP 4400
            <br>
          </p>
          <p class="card-text">
            <abbr title="Phone">P</abbr>: +54 9 387 512-4029
          </p>
          <p>
            <abbr title="Email">E</abbr>:
            <a href="mailto:pabikesalta@gmail.com">pabikesalta@gmail.com
            </a>
          </p>
          <p class="card-text">
            <abbr title="Hours">H</abbr>: Lunes a Viernes: 9:00 AM to 6:00 PM
          </p>
        </div>
      </div>
      <!-- /.row -->
<?php
		$this->load->view("/frontend/formulario-contacto");    
?>
    </div>
    <!-- /.container -->