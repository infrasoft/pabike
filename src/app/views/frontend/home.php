<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('<?=base_url();?>media/img/presentacion/1.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Cuesta del obispo</h3>
              <p>This is a description for the first slide.</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('<?=base_url();?>media/img/presentacion/2.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Camino de cornisa</h3>
              <p>This is a description for the second slide.</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('<?=base_url();?>media/img/presentacion/3.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Circuitos Turisticos</h3>
              <p>This is a description for the third slide.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Siguiente</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4">Bienvenidos a PABIKE SALTA</h1>

      

      <!-- Portfolio Section -->
      <h2>Nuestros Servicios</h2>

      <div class="row">
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="<?=base_url();?>index.php/servicios/detalles/cuesta-del-obispo/">
            	<img class="card-img-top" src="<?=base_url();?>media/img/servicios/cuesta-del-obispo/cuesta-del-obispo.jpg" alt="Cuesta del Obispo"></a>
            <div class="card-body">
              <h4 class="card-title text-justify">
                <a href="<?=base_url();?>index.php/servicios/detalles/cuesta-del-obispo/">Descenso de la Cuesta del Obispo en MTB</a>
              </h4>
              <p class="card-text">10 horas • $1500 x pasajero.</p> 
              <p class="card-text">	 Incluye: Traslado, Bici, Casco, Guantes, Guía, Almuerzo, Hidratacion, Fotos y Seguros Correspondientes. <br/>
              			  Aceptamos tarjetas de Crédito y Débito. </p> 
              <p class="card-text">	  El descenso de la Cuesta del Obispo es una experiencia única, deslumbrante, llena de adrenalina y diversión.
              			   Combinando ruta y sendero recorremos 28 km entre Cuesta y Quebrada de Escoipe.</p>
            </div>
          </div>
        </div>
        
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="<?=base_url();?>index.php/servicios/detalles/selva-de-yungas/">
            	<img class="card-img-top" src="<?=base_url();?>media/img/servicios/camino-de-cornisa/selva-de-yungas.jpg" alt="Selva de yungas">
            </a>
            <div class="card-body">
              <h4 class="card-title text-justify">
                <a href="<?=base_url();?>index.php/servicios/detalles/selva-de-yungas/">Mountain Bike por la Selva de Yungas.</a>
              </h4>
              <p class="card-text">6 horas • $900 x persona. </p>
              <p class="card-text">Incluye: Traslado, Bici, Casco, Guantes, Guía, Almuerzo, Hidratacion, Fotos y Seguros 
              			Correspondientes. Aceptamos tarjetas de Crédito y Débito.</p>
			  <p class="card-text">El Camino de Cornisa por la Selva de Yungas es un lugar magnifico para recorrerlo en Mountain Bike.
			  			 Arboles de gran porte, llanas, enredaderas, epifitas y un pintoresco dique nos acompañan 22 km.</p>
            </div>
          </div>
        </div>
        
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="<?=base_url();?>index.php/servicios/detalles/quebrada-del-toro/">
            	<img class="card-img-top" src="<?=base_url();?>media/img/servicios/quebrada-del-toro/quebrada-del-toro.jpg" alt="Quebrada del toro">
            </a>
            <div class="card-body">
              <h4 class="card-title text-justify">
                <a href="<?=base_url();?>index.php/servicios/detalles/quebrada-del-toro/">Quebrada del Toro</a>
              </h4>
              <p class="card-text">6 horas • $900 x persona. </p>
              <p class="card-text">Incluye: Traslado, Bici, Casco, Guantes, Guía, Almuerzo, Hidratacion, Fotos y Seguros Correspondiente. Aceptamos tarjetas
              			 de Crédito y Débito.</p>
			  <p class="card-text">Desde Estación Chorrillos sobre RN 51 comenzaremos el recorrido hasta Campo Quijano, en unos 25 km, recorreremos la Puna 
			  			 Salteña y el Valle de Lerma, Acompañando las míticas vías del Tren a las Nubes.</p>
            </div>
          </div>
        </div>
        
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="<?=base_url();?>index.php/servicios/detalles/logistica/">
            	<img class="card-img-top" src="<?=base_url();?>media/img/servicios/logistica/logistica.jpg" alt="Logistica">
            </a>
            <div class="card-body">
              <h4 class="card-title text-justify">
                <a href="<?=base_url();?>index.php/servicios/detalles/logistica/">Logistica</a>
              </h4>
              <p class="card-text">Aceptamos tarjetas de Crédito y Débito. </p>
              <p class="card-text">¿Queres recorrer Salta con tu bici o con tu grupo de amigos por tu cuenta?</p>
			  <p class="card-text">Nosotros te recomendamos recorridos, te brindamos apoyo vehicular y todo lo que tenga que ver con logistica. 
			  						Consultanos!!.</p>            </div>
          </div>
        </div>
        
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="<?=base_url();?>index.php/servicios/detalles/rent-a-bike/">
            	<img class="card-img-top" src="<?=base_url();?>media/img/servicios/rent-a-bike/bike.jpg" alt="Rent a Bike">
            </a>
            <div class="card-body">
              <h4 class="card-title text-justify">
                <a href="<?=base_url();?>index.php/servicios/detalles/rent-a-bike/">Rent a Bike</a>
              </h4>
              <p class="card-text">1 hora • $60 x hora $450 full day.  </p>
              <p class="card-text">Consultar precios de Mountain Bike y Equipos para viajes en bicicleta. Aceptamos tarjetas de Crédito
              						 y Débito.</p>
			  <p class="card-text">Destinado a turistas responsables en el uso y cuidado de la bicicleta. Que quieran recorrer atractivos
			  							 por su cuenta y realizar viajes por la provincia de Salta y otros puntos turísticos del NOA.</p>
            </div>
          </div>
        </div>
        
      </div>
      <!-- /.row -->

      

      <hr>

      <!-- Call to Action Section -->
      <div class="row mb-4">
        <div class="col-md-8">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae 
          	veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
        </div>
        <div class="col-md-4">
          <a class="btn btn-lg btn-secondary btn-block" href="#">Mas informacion</a>
        </div>
      </div>
	<?php
		$this->load->view("/frontend/formulario-contacto");    
?>
    </div>
    <!-- /.container -->