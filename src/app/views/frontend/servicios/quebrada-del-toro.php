 <?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Page Content -->
    <div class="container text-justify">
    	<h1 class="mt-4 mb-3">Quebrada del toro</h1>
    	
    	<p><b>Duración estimada:</b> 7 horas aproximadamente</p>
    	
    	<p><b>Grado de Dificultad:</b>baja/media. Es un circuito pensado para realizar un pedaleo liviano sin mucha exigencia en un
    		 paisaje único.</p>
    		 
    	<p><b>Región:</b> Cordillera Oriental. Se denomina Cordillera Oriental al área montañosa de la Argentina que conforma el borde
    		 oriental elevado de la Puna; en las provincias de Jujuy, Salta y un sector al noroeste de Tucumán.</p>
    		 
    	<p><b>Épocas de Salidas:</b></p>
    	    	
    	<p><b>Actividades Incluidas:</b> Ciclismo de montaña, fotografías, visita al pueblo de Campo Quijano donde se empezó la construcción del 
    		Ramal C-14 más conocido como el Tren a las Nubes.</p>
    		
    	<p><b>Equipo:</b>Traslado en vehículo, mountain bike, equipo para andar en bici (cascos, guantes, chaleco reflectivos, 
    		hidratación), guía habilitado, vehículo de apoyo, almuerzo en el pueblo de Campo Quijano, fotos y seguros correspondientes.</p>
    			
    	<p><b>Grupo base – máximo:</b> Grupo base: 2 personas. Grupo máximo: 15 personas</p>
    	
    	<p><b>Horario de salida desde Salta Capital:</b>  9:00 hs; <b>Horario de regreso:</b>  15:30 hs.</p>
    	
    	<p><b>Precio:</b> $1000 x persona.</p>
    	
    	<p><b>Formas de pago:</b></p>
    	
    	<h2>Galeria de Imagenes</h2>
    	<?php 
 		$this->load->view("/frontend/galerias/".$sitio);
		$this->load->view("/frontend/formulario-contacto"); 
    	?>
    </div>
  <!-- End container -->