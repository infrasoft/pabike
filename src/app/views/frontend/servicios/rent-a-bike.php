 <?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Page Content -->
    <div class="container text-justify">
    	<h1 class="mt-4 mb-3">Rent a Bike</h1>
    	<p> El servicio de Rent a Bike está destinado a turistas responsables en el uso y cuidado de la bicicleta. Te brindamos el 
    		equipo necesario tanto para recorrer la ciudad como para realizar viajes largos a los distintos atractivos del norte
    		 argentino. (Mountain Bike, Alforjas, Destelladores, Termos de agua, Cámaras de repuesto, Inflador, Handys, etc.). Tu 
    		 consulta no nos molesta.</p>
    	<?php 
 		$this->load->view("/frontend/galerias/".$sitio);
		$this->load->view("/frontend/formulario-contacto"); 
    	?>
    </div>
  <!-- End container -->