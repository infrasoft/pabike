 <?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Page Content -->
    <div class="container text-justify">
    	<h1 class="mt-4 mb-3">Logistica</h1>
    	<p>Nuestra logística esta apuntada a aquellas personas o grupo de amigos que quieran recorrer nuestra provincia y el norte
    		 argentino con su bicicleta y por sus propios medios.</p>
        <p>Te recomendamos recorridos, tiempo aproximado, apoyo vehicular, equipamiento y todo lo que tenga que ver con tu viaje en 
        	Bicicleta por el norte argentino.</p>
    	<h2>Galeria de Imagenes</h2>
    	<?php 
 		$this->load->view("/frontend/galerias/".$sitio);
		$this->load->view("/frontend/formulario-contacto"); 
    	?>
    </div>
  <!-- End container -->