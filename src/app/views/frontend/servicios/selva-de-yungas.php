 <?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Page Content -->
    <div class="container text-justify">
    	<h1 class="mt-4 mb-3">Camino de Cornisa en Mountain Bike “Selva de Yungas”</h1>
    	
    	<p><b>Duración estimada:</b> 7 horas aproximadamente</p>
    	
    	<p><b>Grado de Dificultad:</b> baja/media. Es un circuito pensado para realizar un pedaleo liviano
sin mucha exigencia en un paisaje único.</p>

    	<p><b>Área geográfica que opera::</b> Selva de yungas. Las selvas de yungas o Selva Tucumano Oranence pertenece a las
    		 provincias de Jujuy, Salta y Tucumán. Se encuentran en las Sierras Sub Andinas y su paisaje es verde todo el año. En 
    		 ella se destacan tres pisos geográficos: Pedemonte, Selva Montana y Nubo Selva con características especificas cada
    		  uno. Su imponente paisaje y el verde de su geografía lo hacen un lugar muy atractivo apto para realizar actividades 
    		  al aire libre como caminatas, cabalgatas, mountain bike, entre otras.</p>
    		  
    	<p><b>Horario de salida desde Salta Capital:</b> 9:00 hs; <b>Horario de regreso:</b>15:30 hs.</p>
    	
    	<p><b>Precio:</b> $100 x persona.</p>
    	
    	<p><b>Actividades Incluidas::</b> Ciclismo de montaña, fotografías, visita a artesanos y puesteros del pueblo de La Caldera 
    		con opción a compra de sus productos.</p>
    		
    	<p><b>Equipo:</b> Traslado en vehículo, mountain bike, equipo para andar en bici (cascos, guantes, chaleco reflectivos,
    		 hidratación), guía habilitado, vehículo de apoyo, almuerzo en el pueblo de La Caldera, fotos y seguros correspondientes.</p>	
    	
    	<p><b>Grupo base – máximo::</b> Grupo base: 2 personas. Grupo máximo: 15 personas</p>
    	
    	<p><b>Descripción del circuito:</b> El camino de Cornisa por la Selva de Yungas es un lugar magnifico para recorrerlo en 
    		Bicicleta. Arboles de gran porte, llanas, enredaderas, un verde intenso y un pintoresco dique entre montañas nos acompañan
    		 por 23 km. Luego de salir en vehículo desde Salta Capital, nos trasladamos hacia el abra de Santa Laura que marca el 
    		 límite entre las provincias de Salta y Jujuy por el camino de cornisa. En este punto se prepara las bicis y los equipos 
    		 para emprender así los 23 km de recorrido hasta el pueblo de La Caldera, acompañados por un guía y una exuberante 
    		 vegetación, realizaremos paradas para tomar fotografías, explicar el paisaje y la importancia de la Selva de Yungas como
    		  regulador natural de los cursos pluviales, hidratarse y observar toda vegetación que nos rodea hasta llegar al pueblito 
    		  de La Caldera.</p>

			<p>En La Caldera llegamos a un parador para almorzar y luego recorrer el pintoresco pueblo con la opción de realizar otras 
				actividades como cabalgatas o simplemente recorrerlo, visitando el paseo costanera donde encontraremos artesanos 
				ofreciendo sus productos terminando así la excursión y emprender el retorno a Salta Capital.</p>
    	<p><b>Formas de pago:</b></p>
    	
    	<h2>Galeria de Imagenes</h2>
    	<?php 
 		$this->load->view("/frontend/galerias/".$sitio);
		$this->load->view("/frontend/formulario-contacto"); 
    	?>
    </div>
  <!-- End container -->