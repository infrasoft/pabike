 <?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
 <!-- Page Content -->
    <div class="container text-justify">
    	<h1 class="mt-4 mb-3">Descenso por cuesta del obispo</h1>
    	<b>Duración Estimada:</b>
    	<p> 10 horas aproximadamente</p>
    	
    	<b>Grado de Dificultad:</b>
    	<p> Baja/Media. Al ser el 85% del circuito en bajada no hace falta un estado físico complejo, solamente 
    		manejando bien la bici podes realizar la excusión.</p>
    	
    	<b>Área Geográfica que opera:</b> 
    	<p>Cordillera Oriental. Se denomina Cordillera Oriental al área montañosa de la Argentina que conforma
    		 el borde oriental elevado de la Puna; en las provincias de Jujuy, Salta y un sector al noroeste de Tucumán.</p>
    		 
    	<b>Servicios:</b><p>Traslado en vehículo, mountain bike, equipo para andar en bici(cascos, guantes, chaleco reflectivos, hidratación), 
    		guía habilitado, vehículo de apoyo, almuerzo en paraje “El Maray”, fotos y seguros correspondientes.</p>
    	
    	<b>Precio:</b>
    	 <p>$1500 x persona.</p>
    	
    	<b>Actividades Incluidas::</b>
    	<p>Ciclismo de montaña, fotografías, visita a artesanos y puesteros de la cuesta con opción a compra de sus productos.</p>
    	
    	<p><b>Equipo:</b></p>	
    	<p><b>¿Que traer ?:</b></p>
    	
    	<b>Descripción de la excursión:</b>
    	<p>Hay experiencias para no olvidar jamás, una de ellas es bajar la imponente Cuesta del Obispo en Mountain Bike, unos de los cordones
			montañosos más hermosos del norte argentino.</p>
    	<p>Luego de salir en vehículo desde Salta Capital nos trasladamos hacia Piedra del Molino, ascendiendo a los 3348 m.s.n.m, siendo esta la 
    		cumbre de la Cuesta del Obispo y el punto más alto del Parque Nacional Los Cardones. En este punto se prepara las bicis y los equipos
    		 para emprender así los primeros 20 km de descenso, acompañados por un guía, a pura adrenalina y diversión, realizando paradas para 
    		 tomar fotografías, explicar el paisaje, hidratarse y contemplar la linda naturaleza que nos rodea hasta llegar al paraje “El Maray” 
    		 (base de la Cuesta).</p>
    	<p>En el Maray nos espera un buen descanso y unas ricas empanadas salteñas para recuperar energías. Luego del almuerzo, continuamos nuestra 
    		aventura en Mountain Bike 7,5 km más por el lecho del Rio Escoipe, que nace en las cumbres de la Cuesta del Obispo y acompaña la 
    		Quebrada de San Fernando de Escoipe. A la altura de la Escuelita Rural de la quebrada de Escoipe conectamos con la ruta provincial 33
    		 donde termina el recorrido en bici con un total de 27.5 km de descenso.</p>
    	<p>Luego de acomodar las bicis y los equipos emprendemos el retorno en vehículo a Salta Capital, realizando algunas paradas para sacar
    		 fotos en puntos específicos de la ruta terminando así un perfecto día en la montaña.</p>
    		 
    	<p><b>Formas de pago:</b></p>
    	
    	<h2>Galeria de Imagenes</h2>
    	<?php 
 		$this->load->view("/frontend/galerias/".$sitio);
		$this->load->view("/frontend/formulario-contacto"); 
    	?>
    </div>
  <!-- End container -->