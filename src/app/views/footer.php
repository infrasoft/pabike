<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
	<!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container row">
      	<div class="col-md-4 col-xs-12 col-sm-4">
      		
      		<h3>Formas de pago</h3>
      		<img class="img-rounded img-fluid" src="<?=base_url();?>media/img/mercadopago.png" alt="Mercado Pago">
      		<a href="<?=base_url();?>">
      			<img class="img-rounded social img-fluid" src="<?=base_url();?>media/img/facebook.jpg" alt="Redes Sociales - cuenta de facebook">
      		</a>
      		
      		<img class="img-rounded social img-fluid" src="<?=base_url();?>media/img/instagram.jpg" alt="Redes Sociales">
      		<img class="img-rounded social img-fluid" src="<?=base_url();?>media/img/youtube.jpg" alt="Redes Sociales">
      	</div>
      	
      	<div class="col-md-4 col-xs-12 col-sm-4">
      		<div id="fb-root"></div>
				<script>(function(d, s, id) {
  				var js, fjs = d.getElementsByTagName(s)[0];
  				if (d.getElementById(id)) return;
  				js = d.createElement(s); js.id = id;
  				js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=244936232620225&autoLogAppEvents=1';
  				fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
      		
      		<div class="fb-page" data-href="https://www.facebook.com/pabikesalta/" data-tabs="timeline" data-width="310" data-height="350" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pabikesalta/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pabikesalta/">Pa&#039; Bike Salta &quot;CicloTurismo de Montaña &amp; Rent a Bike&quot;</a></blockquote></div>
      	</div>
      	
      	<div class="col-md-4 col-xs-12 col-sm-4">
      		<img class="img-rounded" src="<?=base_url();?>media/img/adopta.png" alt="">
      		<img class="logo2 img-rounded img-fluid" src="<?=base_url();?>media/img/caldera.png" alt=""> 
      		
      		
      	</div>
     </div> 	
      <!-- /.container -->
      <div class="container ">
         <p class="m-0 text-center text-white">Copyright &copy; <a href="www.infrasoft.com.ar">Infrasoft Servicios Informaticos - www.infrasoft.com.ar - 2018</a> </p>
	  </div>
    </footer>
</div>
    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url();?>media/css/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url();?>media/css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>