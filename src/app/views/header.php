<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>PABIKE SALTA</title>		
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="author" content="Ariel Marcelo Diaz">
		<link rel="icon" type="image/png" href="<?=base_url();?>media/img/icono.png" />
		<meta name="description" content="<?php if (isset($otros)) 
	 											{
													echo $otros;	 
	 											} ?>">
	   <!-- Bootstrap core CSS -->
   <link href="<?=base_url();?>media/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?=base_url();?>media/css/modern-business.css" rel="stylesheet">
		
		<script src="<?=base_url();?>media/js/scripts.js"></script>
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		

		
		<META name="robots" content="index,follow">
	
	 <?php if (isset($otros)) 
	 	{
			echo $otros;	 
	 	} 
	 ?>
	
	</head>