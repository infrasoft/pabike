/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft
sitio web: http://infrasoft.com.ar
***************************************************/

function validar_venta()
{
  var total = document.getElementById("total");
  document.getElementById("total").value = total.replace(".",",");
  
  var descuento = document.getElementById("descuento");
  document.getElementById("descuento").value = descuento.replace(".",",");
  
  if (isNaN(total))
  {
  	 return false;
  }
  else
  {
  	return true;  	
  }
}